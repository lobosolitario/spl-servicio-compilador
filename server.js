const express = require('express');
const cors = require('cors');
const http = require('http');
const https = require('https');
const Procesador = require('./src/Procesador');
const Ziper = require('./src/model/Descargas/Ziper');
const Ziper2 = require('./src/model/Descargas/Ziper2');

const app = express();
app.use(cors())

app.get('/compila/v1', (req, res) => {
    const procesador = new Procesador();
    procesador.procesar(req, res, function(respuesta){
        res.send(JSON.stringify(respuesta))
    });
});

app.get('/descarga/v1', (req, res) => {
    const procesador = new Procesador();
    procesador.procesar(req, res, function(respuesta){
        let ziper = new Ziper2()
        ziper.crearZip(res, respuesta)
    });
});


app.listen(3101, () => {
    console.log("Servidor iniciado en el puerto dd 3101")
});