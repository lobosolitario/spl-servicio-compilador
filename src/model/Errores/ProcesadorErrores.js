const Log = require("./Log");

function ProcesadorErrores(){


    this.procesarErrorCompilarProductos = function(error, ubicacion){
        let reg =  /at eval \([^\)]*ProcesadorDSL.js[^\)]*\),[^\)]*anonymous>:(\d)*:(\d)*\)/
        return procesarErrorGen(error, ubicacion, reg)
    }

    this.procesarErrorVm = function(error, ubicacion){
        let reg =  /at vm\.js:(\d)*:?(\d)*/
        return procesarErrorGen(error, ubicacion, reg)
    }

    function procesarErrorGen(error, ubicacion, regex1){
        console.log(error)
        let log = new Log()
        let stack = "No controlled error";
        let message = "No controlled error";
        if(typeof error !== 'undefined') {
            stack = String(error)
            message = String(error);
        }else{
            if(typeof error.stack === 'undefined' ){
                stack = String(error)
            }else{
                stack = String(error.stack)
            }
    
            if(typeof error.message === 'undefined' ){
                message = String(error);
            }else{
                message = String(error.message);
            }
        }

        let arrayOfLines = stack.match(/[^\r\n]+/g);
        
        let textoError = "";
        for(var i = 0; i < arrayOfLines.length; i++){
 
            let tag1 = getRegex(arrayOfLines[i],regex1, 1);
            if(tag1 != null){
                log.line = tag1;
                let tag2 = getRegex(arrayOfLines[i],regex1, 2);

                if(tag2 != null){
                    log.column = tag2;
                }

            }else{
                //Se eliminan las ubicaciones de los errores
                let tagAt = getRegex(arrayOfLines[i], /^\s*at/, 0);
                if(tagAt == null){
                    if(arrayOfLines[i] !== ""){
                        textoError = textoError + arrayOfLines[i]  + "\n"
                    }
                    
                  
                }
            }
        }
        log.log = textoError;
        log.type = 2;
        return log
    }

    
    function getRegex(cadena, regex, grupo){
        let tag = cadena.match(regex);
        if(tag != null && tag.length > grupo){
            return tag[grupo];
        }
        return null
    }


}

module.exports = ProcesadorErrores;