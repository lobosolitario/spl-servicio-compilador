function Log(){
    //Logs del objeto consola obtenidos de la ejecucion del codigo del usuario
    //0 = log, 
    //1 = warning, 
    //2 = error 

    //Logs producidos durante la compilacion del proyecto
    //3 = spl error 
    //4 = error codigo compilacion productos
    //5 = error compilacion plantillas
    this.type = 3;


    
    this.log = "";
    this.line = null;
    this.column = null;
    this.errorcode = "";
    this.errorOriginal = "";
}

module.exports = Log;