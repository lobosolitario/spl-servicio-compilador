const fetch = require("node-fetch");

const infoEntorno = {
    loc : {
        conexion : "http://127.0.0.1:8095/api/"
    },
    tst : {
        conexion : "http://jocedo.duckdns.org:8095/api/"
    },
}

const entorno = "tst";
const conexion = infoEntorno[entorno].conexion;


function Cargador(){
    

    this.cargarProyecto = function(callbackOk, callbackNok, idProyecto, idProducto, grupoPlantilla){
        console.log("Cargando proyecto")    
        let url = conexion + "elementos/listarCompiacion/" + idProyecto + "/" + idProducto + "/" + grupoPlantilla 
        fetch(url)
        .then(res => res.json())
        .then(data => {
            console.log("proyecto cargado")
            console.log(data)
            callbackOk(data);
        }).catch(function(e) {
            console.log("Error durante la carga del proyecto")   
            console.log(e)
            callbackNok(e);
        });
        
        
        /*
        const json = null;
        const request = async () => {
            const response = await fetch(conexion + "5102/listar/" + id);
            json = await response.json();

        }
        
        request();
        console.log(json);

        */
    }



    
}





module.exports = Cargador;