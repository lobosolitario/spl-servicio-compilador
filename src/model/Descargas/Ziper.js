var JSZip = require("jszip");
const fs = require('fs');
var FileSaver = require('file-saver');

function Ziper(){
    var data = 'data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAA..kJggg==';


    this.crearZip = function(res, repspuesta, callback){
        var zip = new JSZip();
        zip.file("hello.txt", "Hello World\n");
        var img = zip.folder("images");
        img.file("image.txt", "Imagen");
        zip.generateNodeStream({type:'nodebuffer',streamFiles:true})
        .pipe(fs.createWriteStream('files/out.zip'))
        .on('finish', function () {
            console.log("files/out.zip written.");
        });
    }


}

module.exports = Ziper