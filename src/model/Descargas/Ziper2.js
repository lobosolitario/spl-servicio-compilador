
var archiver = require('archiver');

function Ziper2(){
    var logsFolder = "logs";
    var productFolder = "product"

    this.crearZip = function(res, repspuesta, callback){
        var archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
        });
        
        //response need to be added to the pipe before your add your dynamic content.
        archive.pipe(res)
        
        peocesarRespuesta(res, archive , repspuesta)

        archive.finalize()
        //The critical part that helped is returning the response.
        //return this.response;
    }


    function peocesarRespuesta(res, archive , repspuesta){
        if(repspuesta == null || repspuesta.erroresCompilacion.length > 0){
            res.attachment("error.zip")
            if( repspuesta.erroresCompilacion.length > 0){
                addFile(archive , logsFolder + "/compilation.log", getLogValue(repspuesta.erroresCompilacion))
            }else{
                addFile(archive , logsFolder + "/compilation.log", "Compilation error")
            }
        }else{
            res.attachment("downloaded.zip")
        }
        
        crearFicheros(archive, repspuesta)
    }

    function crearFicheros(archive, repspuesta){
        if(repspuesta != null && repspuesta.proyectoCompilado != null){
            for (let i = 0; i < repspuesta.proyectoCompilado.compilados.length; i++) {
                const compilacion = repspuesta.proyectoCompilado.compilados[i];
                const ruta = productFolder + "/" + compilacion.configuracion.path + "/" + compilacion.configuracion.name
                addFile(archive, ruta , compilacion.code)
                if(compilacion.logs.length > 0){
                    const rutalog = logsFolder + "/" + compilacion.configuracion.path + "/" + compilacion.configuracion.name
                    addFile(archive, rutalog , getLogValue(compilacion.logs))
                }

            }
        }
    }

    function asignarExtensionlog(nombreFichero){
        //TODO:Colocar extension .log para los ficheros logs
    }

    function getLogValue(logs){
        var logString = "";
        for (let i = 0; i < logs.length; i++) {
            logString = logString + logs[i].log + "\n"
        }
        return logString;
    }

    function addFile(archive , ruta, contenido){
        var bufferForFile1 = Buffer.from(contenido);
        archive.append(bufferForFile1, { name: ruta });
    }



}

module.exports = Ziper2