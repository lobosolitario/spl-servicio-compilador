

function Console(){
    var self = this;

    this.logs = [];

    this.log = function(txtlog){

        self.logs.push({
            type : 3, 
            log : txtlog,
            line : null,
            column : null,
            errorcode : "",
            errorOriginal : "",
        })
    }
}

module.exports = Console;