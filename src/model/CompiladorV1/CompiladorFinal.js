const Console = require("../Contextos/Console");
const JavascriptEjecutor = require("../Ejecucion/JavascriptEjecutor");
const ProcesadorErrores = require("../Errores/ProcesadorErrores");
const Compilacion = require("../Respuesta/Compilacion");
const ProyectoCompilado = require("../Respuesta/ProyectoCompilado");
const BuscadorPares = require("./BuscadorPares");
const ConstructorLenguaje = require("./ConstructorLenguaje");
const ProcesadorDSL = require("./ProcesadorDSL");


function CompiladorFinal(){
    let tiempoCompilacionFinal = 1000;
  

    /**
     * El compilador final ejecuta crea un contexto utilizando el modelo semantico obtenido del producto para la ejecucion del codigo de cada una de las plantillas
     */
    this.compilar = function(proyecto, modeloSemantico, plantillasCompiladas, respuesta){
        console.log("Se inicia la compilacion final")
        return compilarPlantillasConProducto(proyecto, modeloSemantico, plantillasCompiladas, respuesta)
    }

    function compilarPlantillasConProducto(proyecto, modeloSemantico, plantillasCompiladas, respuesta){
        let proyectoCompilado = respuesta.proyectoCompilado;
        let buscadorPares = new BuscadorPares();

        let pares = buscadorPares.emparejar(proyecto, modeloSemantico, plantillasCompiladas, respuesta) 


        for(var i = 0; i < pares.length ; i++){
            let compilacion = new Compilacion();
            try{      
                compilacion.idPlantilla = pares[i].plantillaCompilada.plantilla.id;
                compilacion.idProducto = proyecto.productos[0].id;
                compilacion.id = getIdCompilacion(proyectoCompilado,compilacion );
                const start = new Date();
                compilacion.horacompilacion = start.toISOString();
                compilarPlantillaConProducto( proyecto,  pares[i], compilacion, modeloSemantico);
                const end = new Date();
                compilacion.tiempocompilacion = end - start + "ms"
            }catch(error){ 
                console.log(error)
                const procesadorErrores = new ProcesadorErrores();
                const log = procesadorErrores.procesarErrorVm(err)
                log.type = 4;
                log.errorcode = "COMP-PLANT-SCRIPT-ERR"
                compilacion.logs.push(log)
            }
            //TODO _cfg.generate
            proyectoCompilado.compilados.push(compilacion)
        }  
    }
    
    function getIdCompilacion(proyectoCompilado, compilacion){
        //Obtiene un id diferente para cada compilacion en funcion del producto y la plantilla con 
        cont = 0;
        for(var i = 0; i < proyectoCompilado.compilados.length; i++){
            if(compilacion.idProducto == proyectoCompilado.compilados[i].idProducto && compilacion.idPlantilla == proyectoCompilado.compilados[i].idPlantilla){
                cont ++;
            } 
        }
        return compilacion.idProducto + compilacion.idPlantilla + "_" +cont
    }

    function compilarPlantillaConProducto( proyecto,  par, compilacion, modeloSemanticoRoot){
        
        
        console.log("Se inicia la compilacion final:")

        
        let txt = "aa"
        const cons = new Console();
        try{
            //Se inicializa el contexto
            let contexto = {}
            getContextoProductos(proyecto, par, contexto, modeloSemanticoRoot)
            addExtraFunctionsContexto(proyecto,  contexto)

            const jex = new JavascriptEjecutor()
            jex.codigo = par.plantillaCompilada.code;
            jex.contexto = contexto;
            let res = jex.ejecutar();

            compilacion.code = res.xc414956ad65ea23f1774cbc0a138321b93aa2afb6b1e29fcbfecbaa4ecdac5c
            compilacion.logs = res.console.logs;
            compilacion.configuracion = res.$cfg;
            checkearConfig(compilacion.configuracion, par.plantillaCompilada.plantilla)
            //Control del nombre y el path

        }catch(err){
            console.log("Control de errores")
            console.log(err)
            const procesadorErrores = new ProcesadorErrores();
            const log = procesadorErrores.procesarErrorVm(err)
            log.type = 4; //Se setea 4 -> error de compilacion
            log.errorcode = "COMP-JEX-ERR"


            compilacion.configuracion = {
                path : " /error",
                name : par.plantillaCompilada.plantilla.nombre,
                generate : true
            };

            compilacion.logs.push(log)
        }        
    }

    function checkearConfig(cfg, plantilla){
        if(typeof cfg.path === 'undefined' || cfg.path == null ){
            cfg.path = "/"
        }
        
        cfg.path =  normalizaRutas(cfg.path, true)
        
        //Si no se encuentra la barra al inicio se inserta
        if(cfg.path.charAt(0) != "/"){
            cfg.path = "/" + cfg.path
        }

        //Si se encuentran barras al final se elimina
        while(cfg.path.length > 1 && cfg.path.charAt(cfg.path.length -1) == "/"){
            cfg.path = cfg.path.substring(0,cfg.path.length -2)
        }

        //Si no se define el nombre se anyade el nombre por defecto (El de la plantilla)
        if(typeof cfg.name === 'undefined' || cfg.name == null || cfg.name == ""){
            cfg.name = plantilla.nombre;
        }

        cfg.name =  normalizaRutas(cfg.name, false)


        if(typeof cfg.generate === 'undefined' || cfg.generate == null ){
            cfg.generate =true;
        }

    }

    function normalizaRutas(cadena, esdirectorio){
        var caracteresFicherosOrig = ["\\", "/", ":", "*", "?", "\"", "<", ">", "|"]

        var caracteresDirectoriosOrig = ["\\",":", "*", "?", "\"", "<", ">", "|"]

        var res = "";

        var caracteres = caracteresFicherosOrig;
        if(esdirectorio){
            var caracteres = caracteresDirectoriosOrig;
        }

        for (let i = 0; i < cadena.length; i++) {
            //Si algun caracter no se encuentra en el array devuelve el error
            if(!caracteres.some(v  => (v.toUpperCase() == cadena.charAt(i).toUpperCase()) )){
                res = res + cadena.charAt(i)
            }else{
                if(esdirectorio && cadena.charAt(i).toUpperCase() == "\\".toUpperCase()){
                    res = res + "/"
                }
            }
        }

        /*
        if(esdirectorio){
            res = res.replace("\\", "/")
        }
        */
        return res;
    }

    
    function getContextoProductos(proyecto, par, contexto, modeloSemanticoRoot){

        console.log("PAR")
        //Inicializacion del contexto para el procesamiento del producto
        contexto.$self = par.selfObject;
        contexto.$root = par.rootObject;
        contexto.xc414956ad65ea23f1774cbc0a138321b93aa2afb6b1e29fcbfecbaa4ecdac5c = "";
        contexto.$cfg = {
            name : par.plantillaCompilada.plantilla.nombre,
            path : "",
            generate : true
        };

    }
    

    function addExtraFunctionsContexto(proyecto, contexto){
        //Las variables aqui definidas podran ser utilizadas para definir el producto
        //Algunas variables sobreescriben a variables javascript para limitar su uso

        //Sobreescritura de funciones
        contexto["console"] = new Console()
    }


}

module.exports = CompiladorFinal