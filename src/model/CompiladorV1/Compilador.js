const Log = require("../Errores/Log");
const ProcesadorErrores = require("../Errores/ProcesadorErrores");
const CompiladorFinal = require("./CompiladorFinal");
const CompiladorPlantillas = require("./CompiladorPlantillas");
const CompiladorProductos = require("./CompiladorProductos");




function Compilador(){

    this.compilar = function(proyecto, respuesta){

        //Se comprueban los elementos del proyecto
        checkElementos(proyecto, respuesta)

        //Paso 1: Se compila el producto para crear el modelo semantico
        const compiladorProductos = new CompiladorProductos();
        const modeloSemantico = compiladorProductos.compilar(proyecto, respuesta)


        //Paso 2: Se compilan las plantillas para generar el codigo del usuario (Identificacion de scriptlets)
        const compiladorPlantillas = new CompiladorPlantillas();
        const plantillasCompiladas = compiladorPlantillas.compilar(proyecto, respuesta)
        
        //Paso 3: Se compila el modelosemantico junto con las plantillas para obtener el codigo final
        const compiladorFinal = new CompiladorFinal();
        const proyectoCompilado = compiladorFinal.compilar(proyecto, modeloSemantico, plantillasCompiladas, respuesta)


        return proyectoCompilado;
    }


    function checkElementos(proyecto){
        if(proyecto == null){
            throw "ECOMPCHECK01";
        }
        if(proyecto.productos.length <= 0){
            throw "ECOMPCHECK02";
        }
        if(proyecto.plantillas.length <= 0){
            throw "ECOMPCHECK03";
        }
    }




}

module.exports = Compilador