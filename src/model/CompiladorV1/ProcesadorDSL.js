const ModeloSemantico = require("./Data/ModeloSemantico");

function ProcesadorDSL(nodos){
    
    //Se crean los metodos del correspondientes al lenguaje
    
    for(var i = 0; i < nodos.length; i++){ 
        eval(`
            var ` + nodos[i].nombre + ` = function(){
                var nombre = "` + nodos[i].nombre + `";
                var elemento = new ModeloSemantico();
                for (var i=0; i<arguments.length; i++){
                    if(arguments[i] instanceof ModeloSemantico) {
                        elemento.hijos.push(arguments[i])
                    }else{
                        elemento.valores.push(arguments[i])
                    }
                }
                elemento.nombre = nombre;
                return elemento;
            }

        `)     
    }
    
    

    this.procesar = function(code){
        let modelo = eval(code);
        return modelo;
    }
}

module.exports = ProcesadorDSL;