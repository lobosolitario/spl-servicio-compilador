function ConstructorLenguaje(){

    this.getObjetoSelf = function(proyecto, modeloSemantico, selfPadre, deep, index){
        var self = crearElementoLenguaje(proyecto, modeloSemantico, selfPadre,  deep, index);
        console.log("SELF")
        console.log(self)
        return self;
    }


    

    function crearElementoLenguaje(proyecto, modeloSemantico, selfPadre, deep, index){
        console.log("CREANDO SINTAXIS")
        let elementoLenguaje = new ElementoLenguaje();

        //Se crea una referencia al padre
        elementoLenguaje.parent = selfPadre;
  
        if( modeloSemantico.valores.length > 0){
            elementoLenguaje.value = modeloSemantico.valores[0];
        }else{
            elementoLenguaje.value = modeloSemantico.valorDefecto;
        }
    

        //Se crean los arrays de hijos
        var nodo = proyecto.lenguaje.operaciones().getNodoOriginalByName( modeloSemantico.nombre);
        elementoLenguaje.defval = nodo.defecto;

        if(nodo != null){
            //Se crean arrays para cada uno de los hijos posibles
            for(var i = 0 ; i < nodo.hijos.length; i++){
                elementoLenguaje[nodo.hijos[i].nombre] = new Array();
            }
            //Se anyaden los elementos del modelo semantico de forma recursiva
            for(var i = 0 ; i < modeloSemantico.hijos.length; i++){
                var elementoHijo = crearElementoLenguaje(proyecto, modeloSemantico.hijos[i], elementoLenguaje, deep + 1, i);
                elementoLenguaje.all.push(elementoHijo)
                elementoLenguaje[modeloSemantico.hijos[i].nombre].push(elementoHijo)
            }
        }
    
        elementoLenguaje.deep = deep;
        elementoLenguaje.index = index;

        return elementoLenguaje;
    }

    //-----------------------------------------------------


    function ElementoLenguaje(){
        this.value = "";
        this.parent = null;
        this.defval = "";
        this.deep = 0;
        this.index = 0;
        this.all = new Array();
    }

    ElementoLenguaje.prototype.toString = function () {
        return this.defval;
    };

  
    //-----------------------------------------------------

    //Se anyade funcionalidad a los strings en la sintaxis

    String.prototype["capitalize"] = function() {
        return this.charAt(0).toUpperCase() + this.slice(1)
    }

}



module.exports = ConstructorLenguaje