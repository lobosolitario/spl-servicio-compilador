const ConstructorLenguaje = require("./ConstructorLenguaje");
const ParCompilacion = require("./Data/ParCompilacion");

function BuscadorPares(){
    let constructorLenguaje = new ConstructorLenguaje();

    this.emparejar = function(proyecto, modeloSemantico, plantillasCompiladas, respuesta){
        console.log("Iniciando compilacion de pares")
        var pares = [];
        
        examinarModeloSemanticoRecursivo(proyecto, modeloSemantico, plantillasCompiladas, respuesta, "", pares, null, null, 0, 0)
        return pares;

    }


    function examinarModeloSemanticoRecursivo(proyecto, modeloSemantico, plantillasCompiladas, respuesta, rutaModelo, pares, selfPadre, selfRoot, deep, index){

        rutaModelo = rutaModelo + "." + modeloSemantico.nombre

        var selfObject = constructorLenguaje.getObjetoSelf(proyecto, modeloSemantico , selfPadre,  deep, index);
        if(selfRoot == null){
            selfRoot = selfObject;
        }

        //Se comprueba el ancla de cada una de las plantillas en cada modelo
        for (let index = 0; index < plantillasCompiladas.length; index++) {
            if(checkAncla( plantillasCompiladas[index], rutaModelo)){
                var par = new ParCompilacion();
                par.modeloSemantico = modeloSemantico;
                par.plantillaCompilada = plantillasCompiladas[index];
                par.selfObject = selfObject;
                par.rootObject = selfRoot;
                pares.push(par)
            }
        }

        //Busqueda recursiva para cada elemento del modelo
        for (let i = 0; i < modeloSemantico.hijos.length; i++) {
            const hijo = modeloSemantico.hijos[i];
            examinarModeloSemanticoRecursivo(proyecto, hijo, plantillasCompiladas, respuesta, rutaModelo, pares, selfObject, selfRoot, deep + 1, i )
        }

    }

    function checkAncla(plantillaCompilada, rutaModelo){
        var mascara = crearMascara(plantillaCompilada.plantilla.ancla)
  
        var regex = new RegExp(mascara);
        var resultado = rutaModelo.match(regex);
        var mascaraCorrecta = (resultado == rutaModelo);

        return mascaraCorrecta;
    }   


    function crearMascara(ancla){
        var mascara = "";
        //Se divide la mascara por los puntos
        var partes = ancla.split("\.")
        for(var i = 0; i < partes.length; i ++){
            if(partes[i] != undefined && partes[i] != null && partes[i] != ""){
                mascara = mascara + ".*\\." + partes[i];
            }
        }
        mascara = mascara + "$";
        return mascara;
    }

}
module.exports = BuscadorPares;