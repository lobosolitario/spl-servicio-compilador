const Console = require("../Contextos/Console");
const JavascriptEjecutor = require("../Ejecucion/JavascriptEjecutor");
const Log = require("../Errores/Log");
const ProcesadorErrores = require("../Errores/ProcesadorErrores");
const PlantillaCompilada = require("./Data/PlantillaCompilada");
const ProcesadorScriptlets = require("./ProcesadorScriptlests");


function CompiladorPlantillas(){

    /**
     * Devuelve el codigo de las plantillas una vez procesados los scriplets
     */
    this.compilar = function(proyecto, respuesta){
        return compilarPlantilla(proyecto, respuesta)
    }



    function compilarPlantilla(proyecto, respuesta){
        console.log("Se inicia la compilacion de plantillas")

        //Se crea el un objeto para procesar scriptless
        const procesadorScriptlets = new ProcesadorScriptlets();
        let plantillasCompiladas = [];

        for(var i = 0; i < proyecto.plantillas.length ; i++){
            let pc = new PlantillaCompilada();
            pc.plantilla = proyecto.plantillas[i];

            try{
                pc.code = procesadorScriptlets.codificar(proyecto.plantillas[i].code);
            }catch(error){
                console.log(error)
                let log = new Log();
                log.log = "Error processing template"
                log.type = 4;
                log.errorcode = "COMP-PLANT-SCRIPT-ERR"
                pc.err = log;
            }
            plantillasCompiladas.push(pc)
        }   

        return plantillasCompiladas;
    }

}

module.exports = CompiladorPlantillas