const Console = require("../Contextos/Console");
const JavascriptEjecutor = require("../Ejecucion/JavascriptEjecutor");
const ProcesadorErrores = require("../Errores/ProcesadorErrores");
const Compilacion = require("../Respuesta/Compilacion");
const ProcesadorDSL = require("./ProcesadorDSL");
const Validador = require("./Validador");


function CompiladorProductos(){
    let tiempoCompilacionProductos = 1000;
    let modeloSemantico = null;

    /**
     * El compilador de productos crea un contexto en funcion del lenguaje definido por el usuario y ejecuta el codigo del producto para ese lenguaje
     * Finalmente devuelve el modelo semantico definido por el usuario
     */
    this.compilar = function(proyecto, respuesta){
        return compilarProducto(proyecto, respuesta)
    }



    function compilarProducto(proyecto, respuesta){

        console.log("Se inicia la compilacion de productos")

        //Compilar productos
        try{
            //Se construye el contexto
            const contexto = {}
            getContextoProductos(proyecto, contexto, respuesta)
            addExtraFunctionsContexto(proyecto, contexto)

            //Se ejecuta el vodigo para la construccion del modelo semantico
            const jex = new JavascriptEjecutor()
            jex.codigo = "comenzarEjecucion(proyecto)";
            jex.contexto = getContextoProductos(proyecto, contexto);
            const res = jex.ejecutar();

            //Se realiza la validacion del modelo semantico
            validarProducto(modeloSemantico)

            return modeloSemantico;
        }catch(err){
            
            const procesadorErrores = new ProcesadorErrores();
            const log = procesadorErrores.procesarErrorCompilarProductos(err)
            log.type = 4;
            log.errorcode = "COMP-JEX-ERR"

            //Se crea una compilacion para mostrar el error
            let compilacion = new Compilacion();
            compilacion.logs.push(log)
            compilacion.configuracion = {
                path : " /error",
                name : proyecto.productos[0].nombre
            };
            compilacion.id = proyecto.productos[0].id
            respuesta.proyectoCompilado.compilados.push(compilacion)
        }


        return null;
    }

    function validarProducto(){
        const validador = new Validador();
        validador.validar()
    }



    function getContextoProductos(proyecto, contexto, respuesta){

        //Inicializacion del contexto para el procesamiento del producto
        contexto["proyecto"] = proyecto;

        //Metodo principal que realiza el procesado del codigo del producto
        contexto["comenzarEjecucion"] = function (proy){
            //Esta funcion se ejecuta dentro del contexto vm2 de forma segura

            //Se obtiene el producto y el lenguaje
            let producto = proy.productos[0];
            let lenguaje = proyecto.lenguaje;

            //Se prepara el contexto para procesar el proeducto en funcion del lenguaje
            let nodos = lenguaje.operaciones().getArrayNodos()
            let procesadorDSL = new ProcesadorDSL(nodos);

            //Se procesa el producto y se obtiene el modelo semantico asociado, que se extrae a la variable global modeloSemantico
            
            modeloSemantico = procesadorDSL.procesar(producto.code);

            
        }
  
        return contexto
    }


    function addExtraFunctionsContexto(proyecto, contexto){
        //Las variables aqui definidas podran ser utilizadas para definir el producto
        //Algunas variables sobreescriben a variables javascript para limitar su uso

        //Sobreescritura de funciones
        contexto["console"] = new Console()
    }

}

module.exports = CompiladorProductos