function ProcesadorScriptlets(){
    var tokens = [];
    tokens.push(new TokenComment())
    tokens.push(new TokenJSVar())
    tokens.push(new TokenJSCode())

    //Coidifica el codigo recibido
    this.codificar = function(codigo){
        //Se crea el primer bloque para tokenizar
        var bloqueInicial = new Bloque();
        bloqueInicial.code = codigo;
        //Se tokeniza
        var bloques = tokenizar(bloqueInicial, 0)
        //imprimirBloques(bloques)
        //Se evaluan los bloques
        resultado = evaluar(bloques);
        return resultado;
    }

    
    function evaluar(bloques){
        //Se unifican los bloques compilandolos
        //Cada bloque se encarga de transformar su propio codigo si es necesario
        var codigoGenerado = "";
        for(var i = 0; i < bloques.length; i++){
            codigoGenerado = codigoGenerado + bloques[i].getCode();
        }
        //Se crea la plantilla a partir de la evaluacion del codigo enerado  
        //plantilla = ejecutarCodigo(codigoGenerado, contexto);
    
        return codigoGenerado;
    }



    //Devuelve un array de bloques
    function tokenizar(bloque, nivel){
        //console.log("Tokenizando : nivel " + nivel)
        var buscarTokens = 0;
        while(buscarTokens < tokens.length){

            var bloques = tokenizarCodigo(bloque.code,tokens[buscarTokens])

            //Se actualiza el token
            buscarTokens ++;


            //Si se encuentra el token se termina la ejecucion
            if(bloques.length == 4){
                buscarTokens = tokens.length;
            }
        }

        imprimirBloques(bloques)

        var bloquesRetorno = [];

        if(bloques.length == 4){
            //Caso trivial
            //Los bloques que se deben seguir analizando son los bloques 1 y 3
            //El bloque 2 se podria reanalizar solo en el caso de querer iumplementar recursividad (tokens dentro de tokens)
            bloques[1].token = bloque.token;
            bloques[3].token = bloque.token;

           
            gestionarBloquesRetorno(bloquesRetorno, tokenizar(bloques[1], nivel + 1));
            //gestionarBloquesRetorno(bloquesRetorno, tokenizar(bloques[2], nivel + 1));
            
            bloquesRetorno.push(bloques[2])
            gestionarBloquesRetorno(bloquesRetorno, tokenizar(bloques[3], nivel + 1))

        }else{
            bloquesRetorno.push(bloque)
        }
        
        return bloquesRetorno;
    }


    function tokenizarCodigo(code, token){
        var bloques = [];
        var m;
        //La expresion regular dbe de tener tres grupos, siendo el grupo central el correspondienteal codigo obtenido
        var regex = token.regex
        while ((m = regex.exec(code)) !== null) {
            
            
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            
            //Analisis del resultado de la busqueda
            m.forEach(function(match){
                var b = new Bloque();
                b.code = match;
                b.token = token;
                bloques.push(b)
            });
        }

        return bloques;
    }


    function gestionarBloquesRetorno(arryBloques, arrayNuevosBloques){
        for(var i = 0; i < arrayNuevosBloques.length ; i++){
            arryBloques.push(arrayNuevosBloques[i])
        }
    }

    function imprimirBloques(bloques){
        if(bloques.length == 0){
            //console.log("Sin bloques")
        }
        for(var i = 0; i < bloques.length;i++){
            var tipo = "Sin tipo";
            if(bloques[i].token != null){
                tipo = bloques[i].token.nombre;
            }
            //console.log("Bloque " + i + " [" + tipo + "] : " +  bloques[i].code);
        }
    }

}


function Token(){
    this.nombre = "";
    this.regex = "";
}

function TokenJSCode(){
    this.nombre = "CodJS";
    this.regex =  /([\D\d]*)<%([\D\d]*)[^\\]%>([\D\d]*)/gm;

    this.compilar = function(code){
        return  code;
    }
}

function TokenJSVar(){
    this.nombre = "VarJS";
    this.regex =  /([\D\d]*)<%=([^%>]*)%>([\D\d]*)/gm;

    this.compilar = function(code){
        code = code.replace("`"," + String.fromCharCode(96) + `");
        return  `;xc414956ad65ea23f1774cbc0a138321b93aa2afb6b1e29fcbfecbaa4ecdac5c = xc414956ad65ea23f1774cbc0a138321b93aa2afb6b1e29fcbfecbaa4ecdac5c + eval(\``+ code + `\`);`;  
    } 
}

function TokenComment(){
    this.nombre = "comentario";
    this.regex =  /([\D\d]*)<%--([\D\d]*)[^\\]--%>([\D\d]*)/gm;

    this.compilar = function(code){
        return  ``;  
    } 
}

function TokenUserCode(){
    this.nombre = "UserCode";
    this.regex =  /([\D\d]*)/gm;

    this.compilar = function(code){
        code = normalizarCodigo(code)
        return  `;xc414956ad65ea23f1774cbc0a138321b93aa2afb6b1e29fcbfecbaa4ecdac5c = xc414956ad65ea23f1774cbc0a138321b93aa2afb6b1e29fcbfecbaa4ecdac5c + \`` + code + `\`;`;
    }


    function normalizarCodigo(code){
        //Se crea la variable que almacena el codigo procesado
        var newCode = "";
        //Se itera cada uno de los caracteres
        for (let i = 0; i < code.length; i++) {
            if(code.charAt(i) == "\\"){
                //Si se encuentra una barra invertida se duplica
                if(code.charAt(i + 1) == "%"){
                    //Excepcion para las barras invertidas que anulan el %, es necesario que se puedan anular para renderizar scriptlets
                    newCode = newCode + code.charAt(i);
                }else{
                    newCode = newCode + "\\" + code.charAt(i);
                }
            }else{
                //Si se encuentra un apostrofe se coloca una barra invertida delante
                if(code.charCodeAt(i) == 96 ){
                    newCode = newCode + "\\" + code.charAt(i);
                }
                else if(code.charAt(i) == "{"){
                    newCode = newCode + "\\" + code.charAt(i);
                }else{
                    //En cualquier otro caso el caracter no se modifica
                    newCode = newCode + code.charAt(i);
                }
            }
        }
        //Se retorna el nuevo codigo
        return newCode
    }
}



function Bloque(){
    this.procesado = false;
    this.token = new TokenUserCode();
    this.code = "";
    this.getCode = function(){
        return this.token.compilar(this.code);
    }
}


module.exports = ProcesadorScriptlets