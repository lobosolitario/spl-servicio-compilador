const {VM} = require('vm2');

function JavascriptEjecutor(){
    var self = this;

    this.contexto = {};
    this.codigo = null;
    this.maxTime = 10000;


    function checkParams(){
        if(contexto == null){
            return false;
        }
        if(codigo == null){
            return false;
        }
        return true;
    }

    this.ejecutar = function(){
        console.log("Ejecutando vm2")
        //console.log(self.codigo)
        //console.log(self.contexto)
        const vm = new VM({
            sandbox: self.contexto,
            timeout: self.maxTime
        });

        vm.run(self.codigo);  
        console.log("Fin ejecucion vm2")

        return vm.sandbox;
    }
}
module.exports = JavascriptEjecutor;