const ProyectoConversor = require("./splmodulo/Almacenamiento/ProyectoConverosr");
const Cargador = require("./model/Cargador/Cargador");
const Compilador = require("./model/CompiladorV1/Compilador");
const Log = require("./model/Errores/Log");
const Respuesta = require("./model/Respuesta/Respuesta");

const Procesador = function(){

    this.procesar = function(req, res, callback){
        let respuesta = new Respuesta();
        cargarProyecto(req, res, respuesta, callback)
    }
    
    function cargarProyecto(req, res, respuesta, callback){
        
        let cargador = new Cargador();

        cargador.cargarProyecto(
        function(data){
            //Carga de proyecto Ok
            realizarPorceso(req.query.idProyecto, data, respuesta)
            enviarRespuesta(req, res, respuesta, callback)
        },
        function(error){
            crearLog(respuesta, "PRO-CARG-ERR"  , error, "An error occurred during project recovery" )
            enviarRespuesta(req, res, respuesta, callback)
        },
        req.query.idProyecto,
        req.query.idProducto,
        req.query.grupoPlantilla
        
        );
    }

    function enviarRespuesta(req, res, respuesta, callback){
        callback(respuesta)
    }

    function realizarPorceso(id, data, respuesta){
        const proyecto = convertirProyecto(id, data, respuesta)
        compilarProyecto(proyecto, respuesta)
    }


    function convertirProyecto(id, data, respuesta){
        //Convertir el proyecto
        try{
            const proyConversor = new ProyectoConversor();
            return proyConversor.setInfo({id:id}, data);
        } catch(error){
            crearLog(respuesta, "PRO-CONV-ERR" , error, "An error occurred during project recovery")
        }

        return null
    }

    function compilarProyecto(proyecto, respuesta){
        //Compilar el proyecto
        try{
            const compilador = new Compilador();
            return compilador.compilar(proyecto, respuesta)
        } catch(error){
            console.log(error)
            crearLog(respuesta, "PRO-CONP-ERR" , error, "An error occurred while compiling the project" )
        }

        return null;
    }


    function crearLog(respuesta, codigo, original, texto){
        let log = new Log();
        log.log = texto
        log.type = 3;
        log.errorcode = codigo 
        log.errorOriginal = original;
        respuesta.erroresCompilacion.push(log)
    }


}



module.exports = Procesador