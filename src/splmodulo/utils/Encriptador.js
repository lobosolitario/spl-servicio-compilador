const Encriptador = function(){
    
    this.encriptar = function(decoded){
        console.log("ENCRIPTANDO")
        if(typeof decoded === 'undefined' || decoded == null){
            decoded = "";
        }
        const buff = Buffer.from(String(decoded), 'utf-8');
        const b64 = buff.toString('base64')
  
        const uriEncoded = encodeURIComponent(b64)

        return uriEncoded;
    }

    this.desencriptar = function(encoded){

        if(typeof encoded === 'undefined' || encoded == null){
            encoded = "";
        }

        const uriDencoded = decodeURIComponent(encoded)


        const buff = Buffer.from(String(uriDencoded), 'base64');
        return buff.toString('utf-8');
    }
}


module.exports = Encriptador