const Lenguaje = require( "../Proyecto/Lenguaje")
const Plantilla = require( "../Proyecto/Plantilla")
const Producto = require( "../Proyecto/Producto")
const Proyecto = require( "../Proyecto/Proyecto")

const Inicializador = function(){
    var self = this;

    this.inicializarProyecto = function(nombre){
        var proyecto = new Proyecto();
        proyecto.nombre = nombre;
        proyecto.lenguaje = self.inicializarLenguaje();
        return proyecto;
    }

    this.inicializarLenguaje = function(){
        var lenguaje = new Lenguaje();
        return lenguaje;
    }

    this.inicializarPlantilla = function(proyecto, nombre, ancla, grupo){
        var plantilla = new Plantilla();
        plantilla.code = plantilla.id

        if(nombre){
            plantilla.nombre = nombre;
        }else{
            plantilla.nombre = "Nueva plantilla";
        }

        if(ancla){
            plantilla.ancla = ancla;
        }else{
            plantilla.ancla = "root";
        }

        if(grupo){
            plantilla.grupo = grupo;
        }else{
            plantilla.grupo = "";
        }

        return plantilla;
    }

    this.inicializarProducto= function(data, nombre, grupo){
        var producto = new Producto();
        producto.code = producto.id

        if(nombre){
            producto.nombre = nombre;
        }else{
            producto.nombre = "Nuevp producto";
        }


        if(grupo){
            producto.grupo = grupo;
        }else{
            producto.grupo = "";
        }

        return producto;
    }
}
module.exports = Inicializador;