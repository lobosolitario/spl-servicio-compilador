const Commons = require( "../Commons")

const Producto = function(plantilla){
    let self = this;
    
    let commons = new Commons();

    //Id de la plantilla
    this.id = commons.generaID("PROD");

    this.grupo = "";

    this.code = "";

    this.modificado = false;

    this.nombre = "";

}

module.exports = Producto