const Commons = require( "../Commons")
const OperacionesProyecto = require( "../Operaciones/OperacionesProyecto")


const Proyecto = function(proyecto) {
    var self = this;
    const commons = new Commons();

    this.id = -1;
    this.nombre = "";   
  

    this.lenguaje = undefined;
    this.plantillas = new Array();
    this.productos = new Array(); 
    this.eliminados = new Array(); 

    if(proyecto){
        self.id = proyecto.id;
        self.nombre = proyecto.nombre;    
    
        self.lenguaje = proyecto.lenguaje;
        self.plantillas = proyecto.plantillas;
        self.productos = proyecto.productos; 
        self.eliminados = proyecto.eliminados; 

    }

    
    this.getLenguaje = function(){
        return self.lenguaje;
    }

    this.getPlantilla = function(id){
        for(var i = 0; i < self.plantillas.length; i++){
            if(self.plantillas[i].id == id){
                return self.plantillas[i];
            }
        }
        return null
    }

    this.getProducto = function(id){
        return self.productos[0]
    }


    this.operaciones = function(){
        return new OperacionesProyecto(this)
      }


}


module.exports = Proyecto