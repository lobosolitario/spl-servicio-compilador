const OperacionesLenguaje = require( '../Operaciones/OperacionesLenguaje')
const Nodo = require( './Nodo')
const Commons = require( "../Commons")

function Lenguaje(initData) {
    let self = this;

    const commons = new Commons();

    //Id del lenguaje
    this.id = commons.generaID("LENG");;

    //Nodo root
    this.nodo = null;
    

    init();

    /*Inicializa el lenguaje*/
    function init(){
        self.nodo = new Nodo();
        self.nodo.nombre = "product";
        self.nodo.lenguaje = this;
        self.nodo.esReferencia = false;
        self.nodo.esRoot = true;
    }
    

    this.operaciones = function(){
      return new OperacionesLenguaje(this)
    }
}


module.exports = Lenguaje